//
//  ViewController.swift
//  Le4
//
//  Created by student on 27.08.18.
//  Copyright © 2018 lapin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        passwordStrength("QWerty_123456")

    }

    func myNameCount() {
        let name = "Stanislav"
        let numberOfSymbols = name.count
        print ("my name is \(name) and it consists of \(numberOfSymbols) symbols")
    }

    func secondNameCheck() {
        let secondName = "Valeriyovych"
        if secondName.hasSuffix("ych") {
            print ("I am a man \u{1f468}")
        }
        if secondName.hasSuffix("na") {
            print ("I am a woman \u{1f469}")
        }
    }

    func nameDividing1() {
        let name = "StanislavLapin"
        let uppercasedName = name.uppercased()
        var ind = 1
        while name[name.index(name.startIndex, offsetBy: ind)] != uppercasedName[uppercasedName.index(name.startIndex, offsetBy: ind)] {
            ind = ind + 1
        }
        let name1 = name[..<name.index(name.startIndex, offsetBy: ind)]
        let name2 = name[name.index(name.startIndex, offsetBy: ind)..<name.endIndex]
        let name3 = name1 + " " + name2
        print(name1)
        print(name2)
        print(name3)
    }

    func nameDividing2() {
        let name = "StanislavLapin" as NSMutableString
        let uppercasedName = name.uppercased as NSString
        var ind = 1
        while name.character(at: ind) != uppercasedName.character(at: ind) {
            ind = ind + 1
        }
        let name1 = name.substring(to: ind)
        let name2 = name.substring(from: ind)
        name.insert(" ", at: ind)
        print(name1)
        print(name2)
        print(name)
    }

    func mirrorString(_ word: String) {
        var replacingNumber = word.count - 1
        var replacingString = word[word.index(word.endIndex, offsetBy: -1)]
        var mirrorWord = String.init(replacingString)
        for _ in 0..<replacingNumber {
            replacingNumber = replacingNumber - 1
            replacingString = word[word.index(word.startIndex, offsetBy: replacingNumber)]
            mirrorWord.append(replacingString)
        }
        print("string \(word) displayed other way round is \(mirrorWord)")
    }

    func numberDividers(_ number: Int) {
        let numberString = "\(number)"
        let dividedNumber = NSMutableString.init(string: numberString)
        var insertIndex = numberString.count - 3
        while insertIndex > 0 {
            dividedNumber.insert(",", at: insertIndex)
            insertIndex = insertIndex - 3
        }
        print(dividedNumber)
    }

    func passwordStrength(_ password: String) {
        let unicodePassword = Array(password.utf16).description as NSString
        var grade = 0
        let symbols1 = 32...47
        let symbols2 = 58...64
        let symbols3 = 91...96
        let symbols4 = 123...126
        let numbers = 48...57
        let uppercase = 65...90
        let lowercase = 97...122
        func compare(_ range: CountableClosedRange<Int>) {
            for number in range {
                let strNumber = "\(number)"
                if unicodePassword.contains(strNumber) {
                    grade = grade + 1
                    break
                }
            }
        }
        compare(symbols1)
        if grade == 0 {
            compare(symbols2)
        }
        if grade == 0 {
            compare(symbols3)
        }
        if grade == 0 {
            compare(symbols4)
        }
        compare(numbers)
        compare(uppercase)
        compare(lowercase)
        if grade == 4 {
            grade = 5
        }
        print("password strength is \(grade) of 5")
    }





}

